package com.endava.internship.mocking.service;

import com.endava.internship.mocking.model.Payment;
import com.endava.internship.mocking.model.Status;
import com.endava.internship.mocking.model.User;
import com.endava.internship.mocking.repository.PaymentRepository;
import com.endava.internship.mocking.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
class PaymentServiceTest {
    PaymentService testPayService;
    UserRepository userRepository;
    PaymentRepository paymentRepository;
    ValidationService validationService;
    final String messageForSet = "This is new day";
    @Captor
    ArgumentCaptor<Payment> capt;

    @BeforeEach
    void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        paymentRepository = Mockito.mock(PaymentRepository.class);
        validationService = Mockito.mock(ValidationService.class);
        testPayService = new PaymentService(userRepository, paymentRepository, validationService);
    }


    @Test
    void testPaymentRepository() {

        Payment paymentSet = new Payment(2, 43.12, messageForSet);
        paymentSet.setMessage(messageForSet);
        paymentRepository.save(paymentSet);
        Mockito.verify(paymentRepository).save(capt.capture());
        Payment paymentGet = capt.getValue();
        Assertions.assertEquals(paymentSet.getUserId(), paymentGet.getUserId());
        Assertions.assertEquals(paymentSet.getAmount(), paymentGet.getAmount());
        String getMessage = null;
        try {
            Field fildMessage = paymentGet.getClass().getDeclaredField("message");
            fildMessage.setAccessible(true);
            getMessage = (String) fildMessage.get(paymentGet);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assertions.assertEquals(messageForSet, getMessage);
    }

    @Test
    void testValidationServiceMethods() {
        validationService.validateAmount(any());
        Mockito.verify(validationService).validateAmount(any());
        validationService.validateMessage(any());
        Mockito.verify(validationService).validateMessage(any());
        validationService.validatePaymentId(any());
        Mockito.verify(validationService).validatePaymentId(any());
        validationService.validateUser(any());
        Mockito.verify(validationService).validateUser(any());
        validationService.validateUserId(any());
        Mockito.verify(validationService).validateUserId(any());
    }


    @Test
    void createPayment() {
        User user = new User(1, "Alexey", Status.ACTIVE);
        Optional<User> userOpt = Optional.of(user);
        Payment paymentSet = new Payment(1, 24.00, messageForSet);
        Mockito.when(userRepository.findById(any())).thenReturn(userOpt);
        Mockito.when(paymentRepository.save(any())).thenReturn(paymentSet);
        Payment getPay = testPayService.createPayment(1, 24.00);
        Assertions.assertEquals(1, getPay.getUserId());
        Assertions.assertEquals(24.00, getPay.getAmount());
    }

    @Test
    void editMessage() {
        Payment paymentSet = new Payment(1, 24.00, messageForSet);
        Mockito.when(paymentRepository.editMessage(any(),eq(messageForSet))).thenReturn(paymentSet);
        Payment paymentGet = testPayService.editPaymentMessage(any(),messageForSet);
        Assertions.assertEquals(paymentSet.getUserId(),paymentGet.getUserId());
    }

    @Test
    void getAllByAmountExceeding() {
        Payment payment1 = new Payment(1, 24.00, messageForSet);
        Payment payment2 = new Payment(2, 34.00, messageForSet);
        List<Payment> listPayment = new ArrayList<>();
        listPayment.add(payment1);
        listPayment.add(payment2);
        Mockito.when(paymentRepository.findAll()).thenReturn(listPayment);
        List<Payment> listGet = testPayService.getAllByAmountExceeding(24.00);
        Assertions.assertTrue(listGet.contains(payment2));
    }
}
